var osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png');

var ndvi2017 = L.tileLayer('./tiles/ndvi/2017/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var ndvi2018 = L.tileLayer('./tiles/ndvi/2018/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var ndvi2019 = L.tileLayer('./tiles/ndvi/2019/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var ndvi2020 = L.tileLayer('./tiles/ndvi/2020/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var ndvi2021 = L.tileLayer('./tiles/ndvi/2021/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});

var rgb2017 = L.tileLayer('./tiles/rgb/2017/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var rgb2018 = L.tileLayer('./tiles/rgb/2018/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var rgb2019 = L.tileLayer('./tiles/rgb/2019/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var rgb2020 = L.tileLayer('./tiles/rgb/2020/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var rgb2021 = L.tileLayer('./tiles/rgb/2021/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});

var vege2017 = L.tileLayer('./tiles/vege/2017/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var vege2018 = L.tileLayer('./tiles/vege/2018/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var vege2019 = L.tileLayer('./tiles/vege/2019/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var vege2020 = L.tileLayer('./tiles/vege/2020/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});
var vege2021 = L.tileLayer('./tiles/vege/2021/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 10, maxZoom: 14});

current_layer=ndvi2017

var mymap = L.map('mapid', {
    center: [-35.82561193025347, 137.05423366511113],
    // center: [-35.85392882000159, 136.83520783395755],
    zoom: 10,
    minZoom: 10,
    maxZoom: 12,
    layers: [osm,ndvi2017,rgb2017,vege2017]
});



var baseLayers = {
    "RGB": rgb2017,
    "Vegetation index": vege2017,
    "NDVI": ndvi2017
};
L.control.layers(baseLayers).addTo(mymap)



var lll
 mymap.addEventListener('baselayerchange', function (e) {
     console.log('baselayer change')
    // pokud se zmeni layer, zmenime nalepky
    pom=e.layer
    if (pom==ndvi2017){
       lll = {"2017/2018": ndvi2017,"2018/2019": ndvi2018,"2019/2020": ndvi2019,"2020/2021": ndvi2020,"2021/2022": ndvi2021}
    }
    else if (pom==rgb2017){
       lll = {"2017/2018": rgb2017,"2018/2019": rgb2018,"2019/2020": rgb2019,"2020/2021": rgb2020,"2021/2022": rgb2021}
    }
    else {
      lll = {"2017/2018": vege2017,"2018/2019": vege2018,"2019/2020": vege2019,"2020/2021": vege2020,"2021/2022": vege2021}
    }
});


nextMap = function({label} ) {
    // pridava klice mapu pro kazdou vec
    if (!lll) return
    for(var key of Object.keys(lll)){
        if (label==key){
            mymap.removeLayer(current_layer)
            current_layer=lll[key]
            mymap.addLayer(current_layer)
        }
    }
}



L.control.timelineSlider({
    timelineItems: ["2017/2018", "2018/2019", "2019/2020", "2020/2021", "2021/2022"],
    changeMap: nextMap, "labelWidth":"140px"}).addTo(mymap);



var legend = L.control({ position: "bottomleft"});
legend.onAdd = function(mymapdiff) {
    var div = L.DomUtil.create("div", "legend");
    div.innerHTML += "<h4>Legenda";
    div.innerHTML += '<i style="background: #FFFFFF"></i><span>0</span><br>';
    div.innerHTML += '<i style="background: #DF923D"></i><br>';
    div.innerHTML += '<i style="background: #F1B555"></i><br>';
    div.innerHTML += '<i style="background: #FCD163"></i><br>';
    div.innerHTML += '<i style="background: #74A901"></i><br>';
    div.innerHTML += '<i style="background: #529400"></i><span>0.5</span><br>';
    div.innerHTML += '<i style="background: #056201"></i><br>';
    div.innerHTML += '<i style="background: #004C00"></i><br>';
    div.innerHTML += '<i style="background: #012E01"></i><br>';
    div.innerHTML += '<i style="background: #011301"></i><span>1</span><br>';
    div.innerHTML += '<i></i></i><br>';
    div.innerHTML += '<i style="background: #c55939"></i><span>Zdravá vegetace</span><br>';
    div.innerHTML += '<i style="background: #fff42e"></i><span>Středně zdravá vegetace</span><br>';
    div.innerHTML += '<i style="background: #b6fc29"></i><span>Nezdravá vegetace</span><br>';

    return div;
};
legend.addTo(mymap);




