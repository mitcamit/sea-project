
var osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png');

var imp = L.tileLayer('./tiles/impact/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 8, maxZoom: 14})

var reco = L.tileLayer('./tiles/reco/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 8, maxZoom: 14})


var mymapdiff = L.map('ndvidiff', {
    center: [-35.82561193025347, 137.05423366511113],
    zoom: 10,
    minZoom: 10,
    maxZoom: 12,
    layers: [osm,imp]
});

current_layer=imp

var baseLayers = {
    "Impact": imp,
    "Recovery": reco,
};

L.control.layers(baseLayers).addTo(mymapdiff)

var redPallete =
    ["801100",
    "B62203",
    "D73502",
    "FC6400",
    "FF7500",
    "FAC000"];

var palette = ['FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163', '99B718',
    '74A901', '66A000', '529400', '3E8601', '207401', '056201',
    '004C00', '023B01', '012E01', '011D01', '011301'];



    var legendd = L.control({ position: "bottomleft" });
    legendd.onAdd = function(mymapdiff) {
        var div = L.DomUtil.create("div", "legend");
        div.innerHTML += "<h4>Legenda";
        div.innerHTML += '<i style="background: #FF7500"></i><span>Menší pokles vegetace</span></i><br>';
        div.innerHTML += '<i style="background: #D73502"></i><span>Střední pokles vegetace</span></i><br>';
        div.innerHTML += '<i style="background: #801100"></i><span>Větší pokles vegetace</span></i><br>';

        div.innerHTML += '<i></i></i><br>';
        div.innerHTML += '<i style="background: #a8f376"></i><span>Menší míra regenerace</span></i><br>';
        div.innerHTML += '<i style="background: #3E8601"></i><span>Střední míra regenrace</span></i><br>';
        div.innerHTML += '<i style="background: #012E01"></i><span>Větší míra regenrace</span></i><br>';

        return div;
    };
    legendd.addTo(mymapdiff);

