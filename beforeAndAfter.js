var osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png');

var before = L.tileLayer('./tiles/ndvi_before/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 8, maxZoom: 14})

var after = L.tileLayer('./tiles/ndvi_after/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 8, maxZoom: 14})

var mymapbeforeafter = L.map('ndviba', {
    center: [-35.82561193025347, 137.05423366511113],
    zoom: 10,
    minZoom: 10,
    maxZoom: 12,
    layers: [osm,before,after]
});


L.control.sideBySide(before, after).addTo(mymapbeforeafter);


var legend = L.control({ position: "bottomleft"});
legend.onAdd = function(mymapbeforeafter) {
    var div = L.DomUtil.create("div", "legend");
    div.innerHTML += "<h4>Legenda";
    div.innerHTML += '<i style="background: #FFFFFF"></i><span>0</span><br>';
    div.innerHTML += '<i style="background: #DF923D"></i><br>';
    div.innerHTML += '<i style="background: #F1B555"></i><br>';
    div.innerHTML += '<i style="background: #FCD163"></i><br>';
    div.innerHTML += '<i style="background: #74A901"></i><br>';
    div.innerHTML += '<i style="background: #529400"></i><span>0.5</span><br>';
    div.innerHTML += '<i style="background: #056201"></i><br>';
    div.innerHTML += '<i style="background: #004C00"></i><br>';
    div.innerHTML += '<i style="background: #012E01"></i><br>';
    div.innerHTML += '<i style="background: #011301"></i><span>1</span><br>';

    return div;
};
legend.addTo(mymapbeforeafter);