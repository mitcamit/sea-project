var osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png');

var nbr = L.tileLayer('./tiles/nbr/{z}/{x}/{y}.png', {tms: 1, opacity: 1, attribution: "", minZoom: 8, maxZoom: 14})



var mymapnbr = L.map('nbr', {
    center: [-35.82561193025347, 137.05423366511113],
    zoom: 10,
    minZoom: 10,
    maxZoom: 12,
    layers: [osm,nbr]
});
/*
impact = [#ffbaba,
    #ff7b7b,
    #ff5252,
    #ff0000,
    #a70000]
*/
var legend_nbr = L.control({ position: "bottomleft" });
legend_nbr.onAdd = function(mymapnbr) {
    var div = L.DomUtil.create("div", "legend");
    div.innerHTML += "<h4>Legenda";
    div.innerHTML += '<i style="background: #ffbaba"></i><span>Menší výskyt požárů</span></i><br>';
    div.innerHTML += '<i style="background: #ff5252"></i><span>Střední výskyt požárů</span></i><br>';
    div.innerHTML += '<i style="background: #ff0000"></i><span>Větší výskyt požárů</span></i><br>';

    return div;
};
legend_nbr.addTo(mymapnbr);